﻿using Microsoft.AspNetCore.JsonPatch;
using Payment.Business.Enum;
using Payment.Business.Interfaces;
using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Services
{
  public class VendaService : IVendaService
  {
    private readonly IVendaRepository _vendaRepository;
    public VendaService(IVendaRepository vendaRepository)
    {
      _vendaRepository = vendaRepository;

    }
    public void SalvarVenda(Venda venda)
    {
      venda.Status = (int) StatusVenda.AguardandoPagamento;
      _vendaRepository.Adicionar(venda);
    }
    public void Dispose()
    {
      _vendaRepository?.Dispose();
    }

    public async Task AtualizarVenda(Guid Id, int Status)
    {
      var venda = await BuscaVenda(Id);
      if (venda!= null)
      {
        if (venda.Status == (int)StatusVenda.AguardandoPagamento && 
                    (Status == (int)StatusVenda.PagamentoAprovado || 
                     Status == (int)StatusVenda.Cancelado))
        {
          venda.Status = Status;
        }
        if (venda.Status == (int)StatusVenda.PagamentoAprovado &&
                   (Status == (int)StatusVenda.EnviadoTransportadora ||
                    Status == (int)StatusVenda.Cancelado))
        {
          venda.Status = Status;
        }
        if (venda.Status == (int)StatusVenda.EnviadoTransportadora &&
                   (Status == (int)StatusVenda.Entregue))
        {
          venda.Status = Status;
        }
       await  _vendaRepository.Atualizar(venda);
      }
    }

    public async Task<Venda>  BuscaVenda(Guid Id)
    {
      return await _vendaRepository.ObterPorId(Id);
    }
  }
}
