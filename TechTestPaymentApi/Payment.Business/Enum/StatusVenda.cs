﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Enum
{
  public enum StatusVenda
  {
    AguardandoPagamento = 1, 
    PagamentoAprovado = 2,
    EnviadoTransportadora = 3,
    Entregue = 4,
    Cancelado = 5,
  }
}
