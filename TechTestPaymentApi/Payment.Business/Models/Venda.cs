﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Business.Models
{
  public class Venda : Entity
  {

    public Vendedor vendedor { get; set; }
    public DateTime Data { get; set; }
    public List<ItensPedido> itensp { get; set; }
    public int Status { get; set; }

  }

}
