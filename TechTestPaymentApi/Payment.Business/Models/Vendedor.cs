﻿using System;

namespace Payment.Business.Models
{
  public class Vendedor : Entity
  {

    public string Cpf { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
  }
}
