﻿using Microsoft.AspNetCore.JsonPatch;
using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Interfaces
{
  public interface IVendaService : IDisposable
  {
    void SalvarVenda(Venda venda);
    Task AtualizarVenda (Guid Id, int Status);
    Task<Venda> BuscaVenda(Guid Id);
  }


}
