﻿using EasyList.Api.Configurations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Payment.Business.Interfaces;
using Payment.Business.Services;
using Payment.Data.Context;
using Payment.Data.Repository;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Api.Configurations
{
  public static class DependecyInjectionConfig
  {
    public static IServiceCollection ResolveDependecies(this IServiceCollection services)
    {
      services.AddScoped<PaymentDbContext>();
      services.AddScoped<IVendaRepository, VendasRepository>();
      services.AddScoped<IVendaService, VendaService>();
      #region Swagger
      services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
      #endregion Swagger
      return services;
    }


  }
}
