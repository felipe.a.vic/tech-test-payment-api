﻿using AutoMapper;
using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Api.Configurations
{
  public class AutoMapperConfig : Profile
  {
    public AutoMapperConfig()
    {
      CreateMap<Venda, Venda>().ReverseMap();


      CreateMap<ItensPedido, ItensPedido>().ReverseMap();


      CreateMap<Vendedor, Vendedor>().ReverseMap();
    }
  }
}
