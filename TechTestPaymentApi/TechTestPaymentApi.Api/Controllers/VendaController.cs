﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Payment.Business.Interfaces;
using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class VendaController : ControllerBase
  {
    private readonly IVendaService _vendaService;
    private readonly IMapper _mapper;

    public VendaController(IVendaService vendaService, IMapper mapper)
    {
      _vendaService = vendaService;
      _mapper = mapper;

    }

    [HttpGet("{Id}")]
    public async Task<ActionResult<IEnumerable<Venda>>> Venda(Guid Id)
    {
      var vendas = await ObterVendaId(Id);
      if (vendas is null)
        return NotFound();
      return Ok(vendas);
    }
    [HttpPost]
    public async Task<ActionResult<IEnumerable<Venda>>> Venda(Venda venda)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest();
      }

      var vendasEntity = _mapper.Map<Venda>(venda);
      _vendaService.SalvarVenda(vendasEntity);
       
      return CreatedAtAction("Venda", new {Id = venda.Id}, vendasEntity);
    }
    [HttpPost("{Id}/Atualizar")]
    public async Task<ActionResult> AtualizarVenda(Guid Id, int Status)
    {
      if (!ModelState.IsValid)
        return BadRequest();
      _vendaService.AtualizarVenda(Id, Status);


      return Accepted();
    }
    private async Task<Venda> ObterVendaId(Guid Id)
    {
      return _mapper.Map<Venda>(_vendaService.BuscaVenda(Id).Result);
    }

  }
}
