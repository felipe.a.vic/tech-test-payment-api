using AutoMapper;
using EasyList.Api.Configurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Payment.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPaymentApi.Api.Configurations;

namespace TechTestPaymentApi.Api
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }
    
    public IServiceCollection _services { get; set; }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<PaymentDbContext>(options =>
      {
        options.UseInMemoryDatabase("MeDbContextInMemory");
        options.EnableSensitiveDataLogging();
      });
      services.AddControllers().AddNewtonsoftJson();
      services.AddSwaggerConfig();
      services.AddAutoMapper(typeof(Startup));
      services.WebApiConfig();
      services.ResolveDependecies();
      services.AddHealthChecks();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseDeveloperExceptionPage();
      }

      app.UseMvcConfig();
      app.UseSwaggerConfig(provider);

    }
  }
}
