﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Payment.Business.Models;

namespace Payment.Data.Context
{
  public class PaymentDbContext : DbContext
  {
    public PaymentDbContext(DbContextOptions<PaymentDbContext> option) : base(option) { }

    public DbSet<ItensPedido> ItensPedido { get; set; }

    public DbSet<Venda> Venda { get; set; }

    public DbSet<Vendedor> Vendedor { get; set; }
  }
}


