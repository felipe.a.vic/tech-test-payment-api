﻿using Payment.Business.Interfaces;
using Payment.Business.Models;
using Payment.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Data.Repository
{
  public class VendasRepository : Repository<Venda>, IVendaRepository 
  {
    public VendasRepository(PaymentDbContext paymentDbContext) : base(paymentDbContext)
    {
      
    }
  }
}
